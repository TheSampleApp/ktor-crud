package com.thesampleapp.domain

interface UserRepository {
    suspend fun findAll(): List<User>
    suspend fun findById(id: Long): User?
    suspend fun save(user: User): Unit
    suspend fun update(user: User): Unit
    suspend fun deleteById(id: Long): Unit
}