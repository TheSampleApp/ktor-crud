package com.thesampleapp

import com.thesampleapp.plugins.configureUserRouting
import com.thesampleapp.plugins.configureSerialization
import io.ktor.server.application.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module() {
    configureSerialization()
    configureUserRouting()
}
