package com.thesampleapp.plugins

import com.thesampleapp.domain.User
import com.thesampleapp.infra.userRepository
import io.ktor.http.*
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.util.*

fun Application.configureUserRouting() {
    routing {
        route("users"){
            get("{id}") {
                val id = call.parameters.getOrFail<Long>("id").toLong()
                val user =
                    userRepository.findById(id) ?: return@get call.respondText(
                        "No customer with id $id",
                        status = HttpStatusCode.NotFound
                    )
                call.respond(user)
            }
            put("{id}"){
                val id = call.parameters.getOrFail<Long>("id").toLong()
                val formParameters = call.receiveParameters()
                val name = formParameters.getOrFail("name")
                val email = formParameters.getOrFail("email")
                userRepository.update(user = User(id = id, name = name, email = email))
                call.respond(HttpStatusCode.NoContent)
            }
            delete("{id}") {
                val id = call.parameters.getOrFail<Long>("id").toLong()
                userRepository.deleteById(id)
                call.respond(HttpStatusCode.NoContent)
            }
            post{
                val formParameters = call.receiveParameters()
                val name = formParameters.getOrFail("name")
                val email = formParameters.getOrFail("email")
                userRepository.save(user = User(id=null,name = name, email = email))
                call.respond(HttpStatusCode.NoContent)
            }
        }
    }
}
