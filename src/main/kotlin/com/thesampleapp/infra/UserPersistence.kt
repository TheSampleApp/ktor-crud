package com.thesampleapp.infra

import com.thesampleapp.domain.User
import com.thesampleapp.domain.UserRepository
import com.thesampleapp.infra.DatabaseFactory.dbQuery
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.server.config.*
import org.jetbrains.exposed.sql.Table
import kotlinx.coroutines.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.*
import org.jetbrains.exposed.sql.transactions.experimental.*

object Users : Table() {
    val id = long("id").autoIncrement()
    val name = varchar("name", 100)
    val email = varchar("email", 200)
    override val primaryKey = PrimaryKey(id)
}


object DatabaseFactory {
    fun init(config: ApplicationConfig) {
        val driverClassName = config.property("database.driverClassName").getString()
        val jdbcURL = config.property("database.url").getString()
        val username = config.property("database.username").getString()
        val password = config.property("database.password").getString()
        val database = Database.connect(
            createHikariDataSource(
                url = jdbcURL,
                driver = driverClassName,
                aUsername = username,
                aPassword = password
            )
        )
        transaction(database) {
            SchemaUtils.create(Users)
        }
    }

    private fun createHikariDataSource(
        url: String,
        driver: String,
        aUsername: String,
        aPassword: String,
    ) = HikariDataSource(HikariConfig().apply {
        driverClassName = driver
        jdbcUrl = url
        username = aUsername
        password = aPassword
        maximumPoolSize = 3
        isAutoCommit = false
        transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        validate()
    })


    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }
}

class UserRepositoryImpl : UserRepository {
    private fun resultRowToArticle(row: ResultRow) = User(
        id = row[Users.id],
        name = row[Users.name],
        email = row[Users.email],
    )

    override suspend fun findAll(): List<User> = dbQuery {
        Users.selectAll().map(::resultRowToArticle)
    }

    override suspend fun findById(id: Long): User? = dbQuery {
        Users.select { Users.id eq id }.map(::resultRowToArticle).singleOrNull()
    }

    override suspend fun save(user: User) {
        dbQuery {
            val insertStatement = Users.insert {
                it[name] = user.name
                it[email] = user.email
            }
            insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToArticle)
        }
    }

    override suspend fun update(user: User) {
        user.id?.let { id ->
            Users.update({ Users.id eq id }) {
                it[name] = name
                it[email] = email
            } > 0
        }
    }

    override suspend fun deleteById(id: Long) {
        dbQuery {
            Users.deleteWhere { Users.id eq id } > 0
        }
    }

}

val userRepository: UserRepository = UserRepositoryImpl().apply {}